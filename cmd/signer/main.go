package main

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net"
	"net/http"
	"time"

	"github.com/kelseyhightower/envconfig"
	"github.com/piprate/json-gold/ld"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	goahttp "goa.design/goa/v3/http"
	goa "goa.design/goa/v3/pkg"
	"golang.org/x/sync/errgroup"

	"gitlab.eclipse.org/eclipse/xfsc/tsa/golib/auth"
	"gitlab.eclipse.org/eclipse/xfsc/tsa/golib/graceful"
	goahealth "gitlab.eclipse.org/eclipse/xfsc/tsa/signer/gen/health"
	goahealthsrv "gitlab.eclipse.org/eclipse/xfsc/tsa/signer/gen/http/health/server"
	goaopenapisrv "gitlab.eclipse.org/eclipse/xfsc/tsa/signer/gen/http/openapi/server"
	goasignersrv "gitlab.eclipse.org/eclipse/xfsc/tsa/signer/gen/http/signer/server"
	"gitlab.eclipse.org/eclipse/xfsc/tsa/signer/gen/openapi"
	goasigner "gitlab.eclipse.org/eclipse/xfsc/tsa/signer/gen/signer"
	"gitlab.eclipse.org/eclipse/xfsc/tsa/signer/internal/clients/vault"
	"gitlab.eclipse.org/eclipse/xfsc/tsa/signer/internal/config"
	"gitlab.eclipse.org/eclipse/xfsc/tsa/signer/internal/decoder"
	"gitlab.eclipse.org/eclipse/xfsc/tsa/signer/internal/service"
	"gitlab.eclipse.org/eclipse/xfsc/tsa/signer/internal/service/health"
	"gitlab.eclipse.org/eclipse/xfsc/tsa/signer/internal/service/signer"
)

var Version = "0.0.0+development"

func main() {
	// load configuration from environment
	var cfg config.Config
	if err := envconfig.Process("", &cfg); err != nil {
		log.Fatalf("cannot load configuration: %v", err)
	}

	// create logger
	logger, err := createLogger(cfg.LogLevel)
	if err != nil {
		log.Fatalln(err)
	}
	defer logger.Sync() //nolint:errcheck

	logger.Info("signer service started", zap.String("version", Version), zap.String("goa", goa.Version()))

	httpClient := httpClient()

	vault, err := vault.New(cfg.Vault.Addr, cfg.Vault.Token, true, httpClient)
	if err != nil {
		logger.Fatal("cannot initialize vault client", zap.Error(err))
	}

	// create services
	var (
		signerSvc goasigner.Service
		healthSvc goahealth.Service
	)
	{
		// create jsonld document loader which the signer uses to resolve jsonld contexts
		docLoader := ld.NewCachingDocumentLoader(ld.NewDefaultDocumentLoader(httpClient))
		signerSvc = signer.New(vault, cfg.Vault.SupportedKeys, docLoader, logger)
		healthSvc = health.New(Version)
	}

	// create endpoints
	var (
		signerEndpoints  *goasigner.Endpoints
		healthEndpoints  *goahealth.Endpoints
		openapiEndpoints *openapi.Endpoints
	)
	{
		signerEndpoints = goasigner.NewEndpoints(signerSvc)
		healthEndpoints = goahealth.NewEndpoints(healthSvc)
		openapiEndpoints = openapi.NewEndpoints(nil)
	}

	// Provide the transport specific request decoder and response encoder.
	// The goa http package has built-in support for JSON, XML and gob.
	// Other encodings can be used by providing the corresponding functions,
	// see goa.design/implement/encoding.
	var (
		dec = goahttp.RequestDecoder
		enc = goahttp.ResponseEncoder
	)

	// Build the service HTTP request multiplexer and configure it to serve
	// HTTP requests to the service endpoints.
	mux := goahttp.NewMuxer()

	// Wrap the endpoints with the transport specific layers. The generated
	// server packages contains code generated from the design which maps
	// the service input and output data structures to HTTP requests and
	// responses.
	var (
		signerServer  *goasignersrv.Server
		healthServer  *goahealthsrv.Server
		openapiServer *goaopenapisrv.Server
	)
	{
		signerServer = goasignersrv.New(signerEndpoints, mux, dec, enc, nil, errFormatter)
		healthServer = goahealthsrv.New(healthEndpoints, mux, dec, enc, nil, errFormatter)
		openapiServer = goaopenapisrv.New(openapiEndpoints, mux, dec, enc, nil, errFormatter, nil, nil)
	}

	// set custom request decoder, so that request body bytes are simply
	// read and not decoded in some other way.
	// Can these definitions be simplified or taken out into a function for better readability?
	{
		signerServer.VerifyCredential = goasignersrv.NewVerifyCredentialHandler(
			signerEndpoints.VerifyCredential,
			mux,
			decoder.RequestDecoder,
			enc,
			nil,
			errFormatter,
		)

		signerServer.VerifyPresentation = goasignersrv.NewVerifyPresentationHandler(
			signerEndpoints.VerifyPresentation,
			mux,
			decoder.RequestDecoder,
			enc,
			nil,
			errFormatter,
		)
	}

	// Apply Authentication middleware if enabled
	if cfg.Auth.Enabled {
		m, err := auth.NewMiddleware(cfg.Auth.JwkURL, cfg.Auth.RefreshInterval, httpClient)
		if err != nil {
			logger.Fatal("failed to create authentication middleware", zap.Error(err))
		}
		signerServer.Use(m.Handler())
	}

	// Configure the mux.
	goasignersrv.Mount(mux, signerServer)
	goahealthsrv.Mount(mux, healthServer)
	goaopenapisrv.Mount(mux, openapiServer)

	// expose metrics
	go exposeMetrics(cfg.Metrics.Addr, logger)

	var handler http.Handler = mux
	srv := &http.Server{
		Addr:              cfg.HTTP.Host + ":" + cfg.HTTP.Port,
		Handler:           handler,
		ReadHeaderTimeout: cfg.HTTP.ReadTimeout,
		IdleTimeout:       cfg.HTTP.IdleTimeout,
		ReadTimeout:       cfg.HTTP.ReadTimeout,
		WriteTimeout:      cfg.HTTP.WriteTimeout,
	}

	g, ctx := errgroup.WithContext(context.Background())
	g.Go(func() error {
		if err := graceful.Shutdown(ctx, srv, 20*time.Second); err != nil {
			logger.Error("server shutdown error", zap.Error(err))
			return err
		}
		return errors.New("server stopped successfully")
	})
	if err := g.Wait(); err != nil {
		logger.Error("run group stopped", zap.Error(err))
	}

	logger.Info("bye bye")
}

func createLogger(logLevel string, opts ...zap.Option) (*zap.Logger, error) {
	var level = zapcore.InfoLevel
	if logLevel != "" {
		err := level.UnmarshalText([]byte(logLevel))
		if err != nil {
			return nil, err
		}
	}

	config := zap.NewProductionConfig()
	config.Level = zap.NewAtomicLevelAt(level)
	config.DisableStacktrace = true
	config.EncoderConfig.TimeKey = "ts"
	config.EncoderConfig.EncodeTime = zapcore.RFC3339TimeEncoder
	return config.Build(opts...)
}

func errFormatter(ctx context.Context, e error) goahttp.Statuser {
	return service.NewErrorResponse(ctx, e)
}

func httpClient() *http.Client {
	return &http.Client{
		Transport: &http.Transport{
			Proxy: http.ProxyFromEnvironment,
			DialContext: (&net.Dialer{
				Timeout: 30 * time.Second,
			}).DialContext,
			MaxIdleConns:        100,
			MaxIdleConnsPerHost: 100,
			TLSHandshakeTimeout: 10 * time.Second,
			IdleConnTimeout:     60 * time.Second,
		},
		Timeout: 10 * time.Second,
	}
}

func exposeMetrics(addr string, logger *zap.Logger) {
	promMux := http.NewServeMux()
	promMux.Handle("/metrics", promhttp.Handler())
	logger.Info(fmt.Sprintf("exposing prometheus metrics at %s/metrics", addr))
	if err := http.ListenAndServe(addr, promMux); err != nil { //nolint:gosec
		logger.Error("error exposing prometheus metrics", zap.Error(err))
	}
}
