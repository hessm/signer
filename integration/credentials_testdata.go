package integration

var credentialWithSubjectID = `
{
    "@context":
    [
        "https://www.w3.org/2018/credentials/v1",
        "https://w3id.org/security/suites/jws-2020/v1",
        "https://schema.org"
    ],
    "type": "VerifiableCredential",
    "issuer": "did:web:yourdomain.com:policy:policies:example:returnDID:1.0:evaluation",
    "issuanceDate": "2010-01-01T19:23:24Z",
    "credentialSubject":
    {
		"id":"did:web:example.com",
		"allow":true
    }
}`

var credentialWithoutSubjectID = `
{
    "@context":
    [
        "https://www.w3.org/2018/credentials/v1",
        "https://w3id.org/security/suites/jws-2020/v1",
        "https://schema.org"
    ],
    "type": "VerifiableCredential",
    "issuer": "did:web:yourdomain.com:policy:policies:example:returnDID:1.0:evaluation",
    "issuanceDate": "2010-01-01T19:23:24Z",
    "credentialSubject":
    {
		"allow":true
    }
}`

var credentialInvalidSubjectID = `
{
    "@context":
    [
        "https://www.w3.org/2018/credentials/v1",
        "https://w3id.org/security/suites/jws-2020/v1",
        "https://schema.org"
    ],
    "type": "VerifiableCredential",
    "issuer": "did:web:yourdomain.com:policy:policies:example:returnDID:1.0:evaluation",
    "issuanceDate": "2010-01-01T19:23:24Z",
    "credentialSubject":
    {
      "allow":true,
      "id":"invalid"
    }
}`

var credentialWithNumericalSubjectID = `
{
    "@context":
    [
        "https://www.w3.org/2018/credentials/v1",
        "https://w3id.org/security/suites/jws-2020/v1",
        "https://schema.org"
    ],
    "type": "VerifiableCredential",
    "issuer": "did:web:yourdomain.com:policy:policies:example:returnDID:1.0:evaluation",
    "issuanceDate": "2010-01-01T19:23:24Z",
    "credentialSubject":
    {
      "allow":true,
      "id":123
    }
}`
