package vault

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	vaultpkg "github.com/hashicorp/vault/api"

	"gitlab.eclipse.org/eclipse/xfsc/tsa/golib/errors"
	"gitlab.eclipse.org/eclipse/xfsc/tsa/signer/internal/service/signer"
)

const (
	pathSysMounts          = "/v1/sys/mounts"
	defaultTransitSignPath = "/v1/transit/sign/"
)

type Client struct {
	namespace string
	signkey   string
	client    *vaultpkg.Client
}

// New creates a Hashicorp Vault client.
func New(addr string, token string, probe bool, httpClient *http.Client) (*Client, error) {
	cfg := vaultpkg.DefaultConfig()
	cfg.Address = addr
	cfg.HttpClient = httpClient
	client, err := vaultpkg.NewClient(cfg)
	if err != nil {
		return nil, err
	}

	client.SetToken(token)

	// If probe is set, the client will try to query the vault to check if
	// it's unsealed and ready for operation. This is used mostly so unit tests
	// can bypass the check as they don't work against a real Vault.
	if probe {
		if _, err = client.Sys().Capabilities(token, pathSysMounts); err != nil {
			return nil, err
		}
	}

	return &Client{client: client}, nil
}

// WithKey must be called before calling Sign on the client, as each
// request might be used with a different signing key. This function
// will make a new Client wrapper with just the key being different.
func (c *Client) WithKey(namespace, key string) signer.Vault {
	if c.namespace == namespace && c.signkey == key {
		return c
	}

	return &Client{
		signkey:   key,
		namespace: namespace,
		client:    c.client,
	}
}

// Namespaces returns the names of available transit engines in the Vault.
//
// Note: only Vault transit engine names are returned.
func (c *Client) Namespaces(_ context.Context) ([]string, error) {
	engines, err := c.client.Sys().ListMounts()
	if err != nil {
		if e, ok := err.(*vaultpkg.ResponseError); ok {
			return nil, errors.New(errors.GetKind(e.StatusCode), e.Error())
		}
		return nil, err
	}

	var namespaces []string
	for name, engine := range engines {
		if engine.Type == "transit" {
			n := strings.Trim(name, "/")
			namespaces = append(namespaces, n)
		}
	}

	return namespaces, nil
}

// NamespaceKeys returns the names of the keys in a given namespace (vault transit engine).
func (c *Client) NamespaceKeys(ctx context.Context, namespace string) ([]string, error) {
	if namespace == "" {
		return nil, errors.New("keys namespace is missing")
	}

	path := fmt.Sprintf("/v1/%s/keys", namespace)
	req := c.client.NewRequest("LIST", path)
	resp, err := c.client.RawRequestWithContext(ctx, req) //nolint:staticcheck
	if err != nil {
		return nil, errors.New(errors.GetKind(resp.StatusCode), err)
	}
	defer resp.Body.Close()

	var response getKeysResponse
	if err := json.NewDecoder(resp.Body).Decode(&response); err != nil {
		return nil, err
	}

	return response.Data.Keys, nil
}

// Key fetches a key with the given namespace and name from the Vault.
//
// Key namespace correspond 1:1 with a Vault transit engine path.
func (c *Client) Key(ctx context.Context, namespace, key string) (*signer.VaultKey, error) {
	if namespace == "" || key == "" {
		return nil, errors.New("key namespace or name is missing")
	}

	path := fmt.Sprintf("/v1/%s/keys/%s", namespace, key)
	req := c.client.NewRequest(http.MethodGet, path)
	resp, err := c.client.RawRequestWithContext(ctx, req) //nolint:staticcheck
	if err != nil {
		return nil, errors.New(errors.GetKind(resp.StatusCode), err)
	}
	defer resp.Body.Close()

	var response getKeyResponse
	if err := json.NewDecoder(resp.Body).Decode(&response); err != nil {
		return nil, err
	}

	return &signer.VaultKey{
		Name:      response.Data.Name,
		Type:      response.Data.Type,
		PublicKey: response.lastPublicKeyVersion(),
	}, nil
}

// Keys fetches all keys from a given namespace. As the Vault
// doesn't support retrieving all keys at once, the function first
// takes all key names, and then fetches key information for every
// returned key name.
//
// Keys namespace correspond 1:1 with a Vault transit engine path.
func (c *Client) Keys(ctx context.Context, namespace string) ([]*signer.VaultKey, error) {
	if namespace == "" {
		return nil, errors.New("keys namespace is missing")
	}

	path := fmt.Sprintf("/v1/%s/keys", namespace)
	req := c.client.NewRequest("LIST", path)
	resp, err := c.client.RawRequestWithContext(ctx, req) //nolint:staticcheck
	if err != nil {
		return nil, errors.New(errors.GetKind(resp.StatusCode), err)
	}
	defer resp.Body.Close()

	var response getKeysResponse
	if err := json.NewDecoder(resp.Body).Decode(&response); err != nil {
		return nil, err
	}

	var keys []*signer.VaultKey
	for _, keyName := range response.Data.Keys {
		key, err := c.Key(ctx, namespace, keyName)
		if err != nil {
			return nil, err
		}
		keys = append(keys, key)
	}

	return keys, nil
}

// Sign calls the Vault transit API to produce a signature on the given data.
// The key that will be used for signing is determined by the signkey attribute
// of the Client. If you want to sign with a different key, you should call
// client.WithKey before making the Sign. For example:
//
// client := vault.WithKey("transit", "key1")
// client.Sign(...)
func (c *Client) Sign(data []byte) ([]byte, error) {
	body := map[string]interface{}{
		"input": base64.StdEncoding.EncodeToString(data),
	}

	path := fmt.Sprintf("/v1/%s/sign/%s", c.namespace, c.signkey)
	req := c.client.NewRequest(http.MethodPost, path)
	if err := req.SetJSONBody(body); err != nil {
		return nil, err
	}

	resp, err := c.client.RawRequest(req) //nolint:staticcheck
	if err != nil {
		return nil, errors.New(errors.GetKind(resp.StatusCode), err)
	}
	defer resp.Body.Close()

	// expected response from the sign operation
	var response struct {
		Data struct {
			Signature string `json:"signature"`
		} `json:"data"`
	}
	if err := json.NewDecoder(resp.Body).Decode(&response); err != nil {
		return nil, err
	}

	if len(response.Data.Signature) == 0 {
		return nil, fmt.Errorf("unexpected response: no signature")
	}

	// strip the vault specific prefix from the signature, e.g. vault:v1:xxx -> xxx
	// because verifiers are not going to use our Vault for verification, but must
	// verify the "pure" signature on their own.
	var signature string
	s := strings.Split(response.Data.Signature, ":")
	if len(s) > 1 {
		signature = s[len(s)-1]
	} else {
		signature = s[0]
	}

	return base64.StdEncoding.DecodeString(signature)
}

func (c *Client) Alg() string {
	return ""
}
