package signer

import (
	"context"
	"crypto/ed25519"
	"crypto/x509"
	"encoding/base64"
	"encoding/json"
	"encoding/pem"
	"fmt"
	"strings"
	"time"

	"github.com/hyperledger/aries-framework-go/pkg/doc/signature/suite"
	"github.com/hyperledger/aries-framework-go/pkg/doc/signature/suite/jsonwebsignature2020"
	"github.com/hyperledger/aries-framework-go/pkg/doc/util"
	"github.com/hyperledger/aries-framework-go/pkg/doc/verifiable"
	"github.com/hyperledger/aries-framework-go/pkg/vdr"
	"github.com/hyperledger/aries-framework-go/pkg/vdr/key"
	"github.com/hyperledger/aries-framework-go/pkg/vdr/web"
	"github.com/piprate/json-gold/ld"
	"github.com/square/go-jose/v3"
	"go.uber.org/zap"

	"gitlab.eclipse.org/eclipse/xfsc/tsa/golib/errors"
	"gitlab.eclipse.org/eclipse/xfsc/tsa/signer/gen/signer"
)

//go:generate counterfeiter . Vault

var defaultJSONLDContexts = []string{
	"https://www.w3.org/2018/credentials/v1",
	"https://w3id.org/security/suites/jws-2020/v1",
	"https://schema.org",
}

type VaultKey struct {
	Name      string `json:"name"`
	Type      string `json:"type"`
	PublicKey string `json:"public_key,omitempty"`
}

type Vault interface {
	Namespaces(ctx context.Context) ([]string, error)
	NamespaceKeys(ctx context.Context, namespace string) ([]string, error)
	Key(ctx context.Context, namespace, key string) (*VaultKey, error)
	Keys(ctx context.Context, namespace string) ([]*VaultKey, error)
	Sign(data []byte) ([]byte, error)
	WithKey(namespace, key string) Vault
	Alg() string
}

type Service struct {
	vault         Vault
	supportedKeys []string // supported key types
	docLoader     *ld.CachingDocumentLoader
	keyFetcher    verifiable.PublicKeyFetcher

	logger *zap.Logger
}

func New(vault Vault, supportedKeys []string, docLoader *ld.CachingDocumentLoader, logger *zap.Logger) *Service {
	// only DID:WEB and DID:KEY methods are supported currently
	webVDR := web.New()
	keyVDR := key.New()
	registry := vdr.New(
		vdr.WithVDR(webVDR),
		vdr.WithVDR(keyVDR),
	)
	keyResolver := verifiable.NewVDRKeyResolver(registry)

	return &Service{
		vault:         vault,
		supportedKeys: supportedKeys,
		docLoader:     docLoader,
		keyFetcher:    keyResolver.PublicKeyFetcher(),
		logger:        logger,
	}
}

// Namespaces returns all keys namespaces, which corresponds to enabled Vault
// transit engines.
func (s *Service) Namespaces(ctx context.Context) ([]string, error) {
	logger := s.logger.With(zap.String("operation", "namespaces"))

	namespaces, err := s.vault.Namespaces(ctx)
	if err != nil {
		logger.Error("error getting keys namespaces", zap.Error(err))
		return nil, errors.New("error getting keys namespaces", err)
	}

	return namespaces, nil
}

// NamespaceKeys returns all keys names for a given namespace.
func (s *Service) NamespaceKeys(ctx context.Context, req *signer.NamespaceKeysRequest) ([]string, error) {
	logger := s.logger.With(
		zap.String("operation", "namespaceKeys"),
		zap.String("namespace", req.Namespace),
	)

	keys, err := s.vault.NamespaceKeys(ctx, req.Namespace)
	if err != nil {
		if errors.Is(errors.NotFound, err) {
			logger.Error("no namespace keys found", zap.Error(err))
			return nil, errors.New("no keys found in namespace", err)
		}

		logger.Error("error getting namespace keys", zap.Error(err))
		return nil, errors.New("error getting namespace keys", err)
	}

	return keys, nil
}

// VerificationMethod returns a single public key formatted as DID verification method.
func (s *Service) VerificationMethod(ctx context.Context, req *signer.VerificationMethodRequest) (*signer.DIDVerificationMethod, error) {
	logger := s.logger.With(
		zap.String("operation", "verificationMethod"),
		zap.String("namespace", req.Namespace),
		zap.String("key", req.Key),
		zap.String("did", req.Did),
	)

	key, err := s.vault.Key(ctx, req.Namespace, req.Key)
	if err != nil {
		logger.Error("error getting verification method", zap.Error(err))
		return nil, err
	}

	if !s.supportedKey(key.Type) {
		logger.Error("unsupported key type", zap.String("keyType", key.Type))
		return nil, fmt.Errorf("unsupported key type: %s", key.Type)
	}

	pubKey, err := s.jwkFromKey(key)
	if err != nil {
		logger.Error("error making JWK from Vault key",
			zap.String("keyType", key.Type),
			zap.Error(err),
		)
		return nil, fmt.Errorf("error converting vault key to JWK")
	}

	return &signer.DIDVerificationMethod{
		ID:           req.Did + "#" + key.Name,
		Type:         "JsonWebKey2020",
		Controller:   req.Did,
		PublicKeyJwk: pubKey,
	}, nil
}

// VerificationMethods returns all public keys from Vault or OCM.
func (s *Service) VerificationMethods(ctx context.Context, req *signer.VerificationMethodsRequest) (res []*signer.DIDVerificationMethod, err error) {
	logger := s.logger.With(
		zap.String("operation", "verificationMethods"),
		zap.String("namespace", req.Namespace),
		zap.String("did", req.Did),
	)

	keys, err := s.vault.Keys(ctx, req.Namespace)
	if err != nil {
		if !errors.Is(errors.NotFound, err) {
			logger.Error("error getting keys from vault", zap.Error(err))
			return nil, err
		}
		logger.Warn("no keys in vault")
	}

	for _, key := range keys {
		if !s.supportedKey(key.Type) {
			continue
		}

		pubKey, err := s.jwkFromKey(key)
		if err != nil {
			logger.Error("error making JWK from Vault key",
				zap.String("key", key.Name),
				zap.String("keyType", key.Type),
				zap.Error(err),
			)
			return nil, fmt.Errorf("error converting vault key to JWK")
		}

		vm := &signer.DIDVerificationMethod{
			ID:           req.Did + "#" + key.Name,
			Type:         "JsonWebKey2020",
			Controller:   req.Did,
			PublicKeyJwk: pubKey,
		}

		res = append(res, vm)
	}

	return res, nil
}

// JwkPublicKey returns public key by name and namespace.
func (s *Service) JwkPublicKey(ctx context.Context, req *signer.JwkPublicKeyRequest) (any, error) {
	logger := s.logger.With(
		zap.String("operation", "jwkPublicKey"),
		zap.String("namespace", req.Namespace),
		zap.String("key", req.Key),
	)

	key, err := s.vault.Key(ctx, req.Namespace, req.Key)
	if err != nil {
		logger.Error("error getting key", zap.Error(err))
		return nil, err
	}

	pubKey, err := s.jwkFromKey(key)
	if err != nil {
		logger.Error("error converting public key to jwk",
			zap.String("key", key.Name),
			zap.String("keyType", key.Type),
			zap.Error(err),
		)
		return nil, fmt.Errorf("error converting public key to jwk: %v", err)
	}

	return pubKey, nil
}

// CredentialProof adds a proof to a given Verifiable Credential.
func (s *Service) CredentialProof(ctx context.Context, req *signer.CredentialProofRequest) (interface{}, error) {
	logger := s.logger.With(
		zap.String("operation", "credentialProof"),
		zap.String("namespace", req.Namespace),
		zap.String("key", req.Key),
	)

	vcBytes, err := json.Marshal(req.Credential)
	if err != nil {
		logger.Error("credential is not valid json", zap.Error(err))
		return nil, errors.New(errors.BadRequest, err.Error())
	}

	// credential may not have a proof, so disable proofCheck on first round
	vc, err := s.parseCredential(vcBytes, false)
	if err != nil {
		logger.Error("error parsing verifiable credential", zap.Error(err))
		if strings.Contains(err.Error(), "JSON-LD doc has different structure after compaction") {
			return nil, errors.New(errors.BadRequest, "JSON-LD doc has different structure after compaction: some attributes may not be described by schema")
		}
		return nil, errors.New(errors.BadRequest, err.Error())
	}

	// if the given credential has at least one proof, check again to verify the proofs
	if len(vc.Proofs) > 0 {
		vc, err = s.parseCredential(vcBytes, true)
		if err != nil {
			logger.Error("credential proofs cannot be verified", zap.Error(err))
			return nil, errors.New(errors.Forbidden, err.Error())
		}
	}

	if err := validateCredentialSubject(vc.Subject); err != nil {
		logger.Error(err.Error())
		return nil, errors.New(errors.BadRequest, err.Error())
	}

	vcWithProof, err := s.addCredentialProof(ctx, vc.Issuer.ID, req.Namespace, req.Key, vc)
	if err != nil {
		logger.Error("error making credential proof", zap.Error(err))
		return nil, errors.New(err)
	}

	return vcWithProof, nil
}

// PresentationProof adds a proof to a given Verifiable Presentation.
func (s *Service) PresentationProof(ctx context.Context, req *signer.PresentationProofRequest) (interface{}, error) {
	logger := s.logger.With(
		zap.String("operation", "presentationProof"),
		zap.String("namespace", req.Namespace),
		zap.String("key", req.Key),
		zap.String("issuer", req.Issuer),
	)

	vpBytes, err := json.Marshal(req.Presentation)
	if err != nil {
		logger.Error("presentation is not valid json", zap.Error(err))
		return nil, errors.New(errors.BadRequest, err.Error())
	}

	vp, err := verifiable.ParsePresentation(
		vpBytes,
		verifiable.WithPresJSONLDDocumentLoader(s.docLoader),
		verifiable.WithPresStrictValidation(),
	)
	if err != nil {
		logger.Error("error parsing verifiable presentation", zap.Error(err))
		if strings.Contains(err.Error(), "JSON-LD doc has different structure after compaction") {
			return nil, errors.New(errors.BadRequest, "JSON-LD doc has different structure after compaction: some attributes may not be described by schema")
		}
		return nil, errors.New(errors.BadRequest, err.Error())
	}

	if len(vp.Credentials()) == 0 {
		logger.Error("presentation must contain at least 1 verifiable credential")
		return nil, errors.New(errors.BadRequest, "presentation must contain at least 1 verifiable credential")
	}

	for _, cred := range vp.Credentials() {
		cred, ok := cred.(map[string]interface{})
		if !ok {
			logger.Error("presentation has credentials in unsupported format")
			return nil, errors.New(errors.BadRequest, "presentation has credentials in unsupported format")
		}

		credJSON, err := json.Marshal(cred)
		if err != nil {
			logger.Error("fail to encode credential to json", zap.Error(err))
			return nil, errors.New("fail to encode credential to json", err)
		}

		_, err = s.parseCredential(credJSON, true)
		if err != nil {
			logger.Error("error validating credential", zap.Error(err))
			if strings.Contains(err.Error(), "JSON-LD doc has different structure after compaction") {
				return nil, errors.New(errors.BadRequest, "JSON-LD doc has different structure after compaction: some attributes may not be described by schema")
			}
			return nil, errors.New(errors.BadRequest, "error validating credential", err)
		}

		if err := validateCredentialSubject(cred["credentialSubject"]); err != nil {
			logger.Error(err.Error())
			return nil, errors.New(errors.BadRequest, err.Error())
		}
	}

	vpWithProof, err := s.addPresentationProof(ctx, req.Issuer, req.Namespace, req.Key, vp)
	if err != nil {
		logger.Error("error making presentation proof", zap.Error(err))
		return nil, errors.New(err)
	}

	return vpWithProof, nil
}

// CreateCredential creates Verifiable Credential with proof from raw JSON data.
func (s *Service) CreateCredential(ctx context.Context, req *signer.CreateCredentialRequest) (interface{}, error) {
	logger := s.logger.With(
		zap.String("operation", "createCredential"),
		zap.String("namespace", req.Namespace),
		zap.String("key", req.Key),
		zap.String("issuer", req.Issuer),
	)

	if req.CredentialSubject == nil {
		logger.Error("invalid or missing credential subject")
		return nil, errors.New(errors.BadRequest, "invalid or missing credential subject")
	}

	credSubject, ok := req.CredentialSubject.(map[string]interface{})
	if !ok || len(credSubject) == 0 {
		logger.Error("invalid credential subject: non-empty map is expected")
		return nil, errors.New(errors.BadRequest, "invalid credential subject: non-empty map is expected")
	}

	// add additional jsonld contexts only if they are different from the default
	jsonldContexts := defaultJSONLDContexts
	for _, jsonldContext := range req.Context {
		if !containContext(defaultJSONLDContexts, jsonldContext) {
			jsonldContexts = append(jsonldContexts, jsonldContext)
		}
	}

	var subject verifiable.Subject
	if subjectID, ok := credSubject["id"].(string); ok && len(subjectID) > 0 {
		subject.ID = subjectID
		delete(credSubject, "id")
	}
	subject.CustomFields = credSubject

	vc := &verifiable.Credential{
		Context: jsonldContexts,
		Types:   []string{verifiable.VCType},
		Issuer:  verifiable.Issuer{ID: req.Issuer},
		Issued:  &util.TimeWrapper{Time: time.Now()},
		Subject: subject,
	}

	err := validateCredentialSubject(vc.Subject)
	if err != nil {
		logger.Error("invalid credential subject", zap.Error(err))
		return nil, errors.New(errors.BadRequest, err)
	}

	vcWithProof, err := s.addCredentialProof(ctx, req.Issuer, req.Namespace, req.Key, vc)
	if err != nil {
		logger.Error("error making credential proof", zap.Error(err))
		return nil, err
	}

	return vcWithProof, nil
}

// CreatePresentation creates VP with proof from raw JSON data.
func (s *Service) CreatePresentation(ctx context.Context, req *signer.CreatePresentationRequest) (interface{}, error) {
	logger := s.logger.With(
		zap.String("operation", "createPresentation"),
		zap.String("namespace", req.Namespace),
		zap.String("key", req.Key),
		zap.String("issuer", req.Issuer),
	)

	if req.Data == nil || len(req.Data) == 0 {
		logger.Error("invalid or missing credentials data")
		return nil, errors.New(errors.BadRequest, "invalid or missing credentials data")
	}

	// prepare credentials to be included in the VP
	var credentials []*verifiable.Credential
	for _, credData := range req.Data {
		credSubject, ok := credData.(map[string]interface{})
		if !ok {
			logger.Error("invalid credential data: map is expected")
			return nil, errors.New(errors.BadRequest, "invalid credential data: map is expected")
		}

		vc := &verifiable.Credential{
			Context: defaultJSONLDContexts,
			Types:   []string{verifiable.VCType},
			Issuer:  verifiable.Issuer{ID: req.Issuer},
			Issued:  &util.TimeWrapper{Time: time.Now()},
			Subject: verifiable.Subject{
				CustomFields: credSubject,
			},
		}

		if err := validateCredentialSubject(vc.Subject); err != nil {
			logger.Error(err.Error())
			return nil, errors.New(errors.BadRequest, err.Error())
		}

		credentials = append(credentials, vc)
	}

	vp, err := verifiable.NewPresentation(verifiable.WithCredentials(credentials...))
	if err != nil {
		logger.Error("error making verifiable presentation", zap.Error(err))
		return nil, err
	}

	// add additional jsonld contexts only if they are different from the default
	jsonldContexts := defaultJSONLDContexts
	for _, jsonldContext := range req.Context {
		if !containContext(defaultJSONLDContexts, jsonldContext) {
			jsonldContexts = append(jsonldContexts, jsonldContext)
		}
	}

	vp.Context = jsonldContexts
	vp.ID = req.Issuer
	vp.Type = []string{verifiable.VPType}

	vpWithProof, err := s.addPresentationProof(ctx, req.Issuer, req.Namespace, req.Key, vp)
	if err != nil {
		logger.Error("error making presentation proof", zap.Error(err))
		return nil, err
	}

	return vpWithProof, nil
}

// VerifyCredential verifies the proof of a Verifiable Credential.
func (s *Service) VerifyCredential(_ context.Context, req *signer.VerifyCredentialRequest) (*signer.VerifyResult, error) {
	logger := s.logger.With(zap.String("operation", "verifyCredential"))

	// verify credential
	vc, err := s.parseCredentialWithProof(req.Credential)
	if err != nil {
		logger.Error("error verifying credential", zap.Error(err))
		if strings.Contains(err.Error(), "JSON-LD doc has different structure after compaction") {
			return nil, errors.New(errors.BadRequest, "JSON-LD doc has different structure after compaction: some attributes may not be described by schema")
		}
		return nil, errors.New(errors.BadRequest, err.Error())
	}

	if err := validateCredentialSubject(vc.Subject); err != nil {
		logger.Error(err.Error())
		return nil, errors.New(errors.BadRequest, err.Error())
	}

	return &signer.VerifyResult{Valid: true}, nil
}

// VerifyPresentation verifies the proof of a Verifiable Presentation.
func (s *Service) VerifyPresentation(_ context.Context, req *signer.VerifyPresentationRequest) (*signer.VerifyResult, error) {
	logger := s.logger.With(zap.String("operation", "verifyPresentation"))

	// verify presentation
	vp, err := verifiable.ParsePresentation(
		req.Presentation,
		verifiable.WithPresPublicKeyFetcher(s.keyFetcher),
		verifiable.WithPresEmbeddedSignatureSuites(
			jsonwebsignature2020.New(suite.WithVerifier(jsonwebsignature2020.NewPublicKeyVerifier())),
		),
		verifiable.WithPresJSONLDDocumentLoader(s.docLoader),
		verifiable.WithPresStrictValidation(),
	)
	if err != nil {
		logger.Error("error verifying presentation", zap.Error(err))
		if strings.Contains(err.Error(), "JSON-LD doc has different structure after compaction") {
			return nil, errors.New(errors.BadRequest, "JSON-LD doc has different structure after compaction: some attributes may not be described by schema")
		}
		return nil, errors.New(errors.BadRequest, err.Error())
	}

	// check if the credential contains proof section
	if len(vp.Proofs) == 0 {
		logger.Error("verifiable presentation must have proof section")
		return nil, errors.New(errors.BadRequest, "verifiable presentation must have proof section")
	}

	if len(vp.Credentials()) == 0 {
		logger.Error("presentation must contain at least 1 verifiable credential")
		return nil, errors.New(errors.BadRequest, "presentation must contain at least 1 verifiable credential")
	}

	for _, cred := range vp.Credentials() {
		cred, ok := cred.(map[string]interface{})
		if !ok {
			logger.Error("presentation has credentials in unsupported format")
			return nil, errors.New(errors.BadRequest, "presentation has credentials in unsupported format")
		}

		credJSON, err := json.Marshal(cred)
		if err != nil {
			logger.Error("fail to encode credential to json", zap.Error(err))
			return nil, errors.New("fail to encode credential to json", err)
		}

		_, err = s.parseCredential(credJSON, true)
		if err != nil {
			logger.Error("error validating credential", zap.Error(err))
			if strings.Contains(err.Error(), "JSON-LD doc has different structure after compaction") {
				return nil, errors.New(errors.BadRequest, "JSON-LD doc has different structure after compaction: some attributes may not be described by schema")
			}
			return nil, errors.New(errors.BadRequest, err.Error())
		}

		if err := validateCredentialSubject(cred["credentialSubject"]); err != nil {
			logger.Error(err.Error())
			return nil, errors.New(errors.BadRequest, err.Error())
		}
	}

	return &signer.VerifyResult{Valid: true}, nil
}

// Sign creates digital signature on base64 encoded binary data.
func (s *Service) Sign(_ context.Context, req *signer.SignRequest) (res *signer.SignResult, err error) {
	logger := s.logger.With(
		zap.String("operation", "sign"),
		zap.String("namespace", req.Namespace),
		zap.String("key", req.Key),
	)

	data, err := base64.StdEncoding.DecodeString(req.Data)
	if err != nil {
		logger.Error("cannot base64 decode data", zap.Error(err))
		return nil, errors.New(errors.BadRequest, "cannot base64 decode data", err)
	}

	signature, err := s.vault.WithKey(req.Namespace, req.Key).Sign(data)
	if err != nil {
		logger.Error("error signing data", zap.Error(err))
		return nil, errors.New(err)
	}

	encodedSignature := base64.StdEncoding.EncodeToString(signature)

	return &signer.SignResult{Signature: encodedSignature}, nil
}

func (s *Service) supportedKey(keyType string) bool {
	for _, kt := range s.supportedKeys {
		if kt == keyType {
			return true
		}
	}
	return false
}

func (s *Service) parseCredential(vc []byte, proofCheck bool) (*verifiable.Credential, error) {
	var opts []verifiable.CredentialOpt
	opts = append(opts, verifiable.WithJSONLDDocumentLoader(s.docLoader))
	opts = append(opts, verifiable.WithStrictValidation())

	if proofCheck {
		opts = append(opts, verifiable.WithPublicKeyFetcher(s.keyFetcher))
		opts = append(opts, verifiable.WithEmbeddedSignatureSuites(
			jsonwebsignature2020.New(suite.WithVerifier(jsonwebsignature2020.NewPublicKeyVerifier())),
		))
	} else {
		opts = append(opts, verifiable.WithDisabledProofCheck())
	}

	return verifiable.ParseCredential(vc, opts...)
}

func (s *Service) parseCredentialWithProof(vc []byte) (*verifiable.Credential, error) {
	cred, err := verifiable.ParseCredential(
		vc,
		verifiable.WithPublicKeyFetcher(s.keyFetcher),
		verifiable.WithEmbeddedSignatureSuites(
			jsonwebsignature2020.New(suite.WithVerifier(jsonwebsignature2020.NewPublicKeyVerifier())),
		),
		verifiable.WithJSONLDDocumentLoader(s.docLoader),
		verifiable.WithStrictValidation(),
	)
	if err != nil {
		return nil, err
	}

	// check if the credential contains proof section
	if len(cred.Proofs) == 0 {
		return nil, errors.New("verifiable credential must have proof section")
	}

	return cred, nil
}

func (s *Service) jwkFromKey(key *VaultKey) (*jose.JSONWebKey, error) {
	k := &jose.JSONWebKey{
		KeyID: key.Name,
	}

	switch key.Type {
	case "ed25519":
		pk, err := base64.StdEncoding.DecodeString(key.PublicKey)
		if err != nil {
			return nil, fmt.Errorf("jwkFromKey: failed to decode ed25519 key: %v", err)
		}
		k.Key = ed25519.PublicKey(pk)
	case "ecdsa-p256", "ecdsa-p384", "ecdsa-p521", "rsa-2048", "rsa-3072", "rsa-4096":
		block, _ := pem.Decode([]byte(key.PublicKey))
		if block == nil {
			return nil, fmt.Errorf("jwkFromKey: no public key found during PEM decode")
		}

		pub, err := x509.ParsePKIXPublicKey(block.Bytes)
		if err != nil {
			return nil, err
		}
		k.Key = pub
	default:
		return nil, fmt.Errorf("jwkFromKey: unsupported key type: %s", key.Type)
	}

	return k, nil
}

func validateCredentialSubject(subject interface{}) error {
	if subject == nil {
		return fmt.Errorf("verifiable credential must have subject")
	}

	switch subj := subject.(type) {
	case []verifiable.Subject:
		for _, sub := range subj {
			if sub.ID != "" {
				err := validateSubjectID(sub.ID)
				if err != nil {
					return err
				}
			}
		}
	case verifiable.Subject:
		if subj.ID != "" {
			err := validateSubjectID(subj.ID)
			if err != nil {
				return err
			}
		}
	case map[string]interface{}:
		if subj["id"] != nil {
			id, ok := subj["id"].(string)
			if !ok {
				return fmt.Errorf("invalid subject id format, string is expected")
			}
			err := validateSubjectID(id)
			if err != nil {
				return err
			}
		}
	default:
		return fmt.Errorf("unknown credential subject format")
	}

	return nil
}

func validateSubjectID(id string) error {
	s := strings.Split(id, ":")
	if len(s) < 2 {
		return fmt.Errorf("invalid subject id: must be URI")
	}

	if len(s[0]) == 0 || len(s[1]) == 0 {
		return fmt.Errorf("invalid subject id: must be URI")
	}

	return nil
}

func containContext(contexts []string, context string) bool {
	for _, c := range contexts {
		if c == context {
			return true
		}
	}
	return false
}
